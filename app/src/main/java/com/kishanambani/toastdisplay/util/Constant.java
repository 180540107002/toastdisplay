package com.kishanambani.toastdisplay.util;

public class Constant {
    public static final String FIRST_NAME = "Firstname";
    public static final String LAST_NAME = "Lastname";
    public static final String GENDER = "Gender";
    public static final String HOBBY = "Hobby";
}
