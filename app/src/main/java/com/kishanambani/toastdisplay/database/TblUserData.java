package com.kishanambani.toastdisplay.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

public class TblUserData extends MyDatabase {
    public static final String TABLE_NAME = "Tbl_UserData";
    public static final String USER_NAME = "Name";
    public static final String PHONE_NUMBER = "PhoneNumber";
    public static final String EMAIL = "Email";

//    public static final String USER_NAME="Name";

    public TblUserData(Context context) {
        super(context);
    }

    public long insertUser(String name, String num, String email) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(USER_NAME, name);
        cv.put(PHONE_NUMBER, num);
        cv.put(EMAIL, email);
        long counter = db.insert(TABLE_NAME, null, cv);
        db.close();
        return counter;
    }

    public ArrayList<HashMap<String, Object>> getTblData() {
        ArrayList<HashMap<String, Object>> userList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        String[] data;
        String query = "Select * from " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        if (cursor != null) {
            while (!cursor.isAfterLast()) {
                Log.d("DATA :::", "" + cursor.getString(1));
                Log.d("DATA :::", "" + cursor.getString(2));
                Log.d("DATA :::", "" + cursor.getString(3));
                HashMap<String, Object> map = new HashMap<>();
                map.put("NAME", "" + cursor.getString(1));
                map.put("PhoneNumber", "" + cursor.getString(2));
                map.put("Email", "" + cursor.getString(3));
                Log.d("MAP DATA :::", "" + map.get("NAME"));
                Log.d("MAP DATA :::", "" + map.get("PhoneNumber"));
                Log.d("MAP DATA :::", "" + map.get("Email"));
                userList.add(map);
                cursor.moveToNext();
            }
            cursor.close();
        }
        db.close();
        return userList;
    }
}
