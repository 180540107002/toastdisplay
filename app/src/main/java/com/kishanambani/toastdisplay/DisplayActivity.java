package com.kishanambani.toastdisplay;

import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.kishanambani.toastdisplay.Adapter.UserListAdapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class DisplayActivity extends AppCompatActivity {
    ListView lstviewDisplayUser;
    ArrayList<HashMap<String, Object>> userList = new ArrayList<>();
    UserListAdapter userListAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);
        initValue();
    }

    public void initValue() {
        lstviewDisplayUser = findViewById(R.id.lstviewActDisplayUser);
        userList.addAll((Collection<? extends HashMap<String, Object>>) getIntent().getSerializableExtra("UserList"));
        Log.d("DISPLAY ACTIVITY :::", "" + userList.get(1).get("NAME"));
        userListAdapter = new UserListAdapter(this, userList);
        lstviewDisplayUser.setAdapter(userListAdapter);
    }
}
