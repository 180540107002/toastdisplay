package com.kishanambani.toastdisplay;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.kishanambani.toastdisplay.database.MyDatabase;
import com.kishanambani.toastdisplay.database.TblUserData;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    EditText etFirstname, etLastname, etPhonenumber, etEmail;
    RadioButton rbtnFemale, rbtnMale, rbtnSelected;
    Button btnSubmit;
    RadioGroup rg;
    CheckBox chkboxCricket, chkboxFootball, chkboxHockey;
    ArrayList<HashMap<String, Object>> userList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                int selectedId = rg.getCheckedRadioButtonId();
                rbtnSelected = (RadioButton) findViewById(selectedId);
                if (rbtnSelected.getText().toString().equals("Female")) {
                    chkboxHockey.setVisibility(View.GONE);
                } else {
                    chkboxHockey.setVisibility(View.VISIBLE);
                }
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                HashMap<String, Object> map = new HashMap<>();
//                map.put(Constant.FIRST_NAME, etFirstname.getText().toString());
//                map.put(Constant.LAST_NAME, etLastname.getText().toString());
//                map.put(Constant.GENDER, rbtnSelected.getText().toString());
//                map.put(Constant.HOBBY, getHobbys());
//                userList.add(map);

                TblUserData tblUserData = new TblUserData(MainActivity.this);
                long flag = tblUserData.insertUser(etFirstname.getText().toString() + " " + etLastname.getText().toString(), etPhonenumber.getText().toString(), etEmail.getText().toString());
                Toast.makeText(MainActivity.this, flag > 0 ? "User Inserted Successfully" : "Something Went Wrong", Toast.LENGTH_SHORT).show();
                Intent display = new Intent(getApplicationContext(), DisplayActivity.class);
                display.putExtra("UserList", tblUserData.getTblData());
                startActivity(display);

            }
        });
    }

    public void initView() {
        new MyDatabase(MainActivity.this).getReadableDatabase();
        etFirstname = findViewById(R.id.etActFirstname);
        etLastname = findViewById(R.id.etActLastname);
        rbtnFemale = findViewById(R.id.rbtnActFemale);
        rbtnMale = findViewById(R.id.rbtnActMale);
        btnSubmit = findViewById(R.id.btnActSubmit);
        rg = findViewById(R.id.rdgrpActGender);
        chkboxCricket = findViewById(R.id.chkboxActCricket);
        chkboxFootball = findViewById(R.id.chkboxActFootball);
        chkboxHockey = findViewById(R.id.chkboxActHockey);
        etPhonenumber = findViewById(R.id.etActPhoneNumber);
        etEmail = findViewById(R.id.etActEmail);
    }

    public String getHobbys() {
        String hobbys = "";
        int flag = 0;
        if (chkboxHockey.isChecked()) {
            hobbys += "," + chkboxHockey.getText().toString();
            flag++;

        }
        if (chkboxFootball.isChecked()) {
            hobbys += "," + chkboxFootball.getText().toString();
            flag++;
        }
        if (chkboxCricket.isChecked()) {
            hobbys += "," + chkboxCricket.getText().toString();
            flag++;
        }
        if (flag > 0) {
            return hobbys.substring(1);
        } else {
            return "No Selected Hobby";
        }
    }

}